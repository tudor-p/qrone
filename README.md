# [QR One](https://bitbucket.org/tudor-p/qrone) - one single HTML 5 file to scan and generate QR codes
Real-time webcam-driven HTML5 QR code scanner and generator.
[Try the live demo at (https://tudor-p.bitbucket.io/qrone/](https://tudor-p.bitbucket.io/qrone/).
## About
Merger project oriented on providing quick and integrated QR scanning and generating services based on two existing components:
 - [Instascan](https://github.com/schmich/instascan) for scanning QR codes with the integrated webcam
 - [QRCode.js](https://github.com/davidshimjs/qrcodejs) for generating QR codes
## Performance pointers (inerited from Instascan project)
Many factors affect how quickly and reliably Instascan component can detect QR codes.

If you control creation of the QR code, consider the following:

- A larger physical code is better. A 2" square code is better than a 1" square code.
- Flat, smooth, matte surfaces are better than curved, rough, glossy surfaces.
- Include a sufficient quiet zone, the white border surrounding QR code. The quiet zone should be at least four times the width of an individual element in your QR code.
- A simpler code is better. You can use [this QR code generator](https://www.the-qrcode-generator.com/) to see how your input affects complexity.
- For the same length, numeric content is simpler than ASCII content, which is simpler than Unicode content.
- Shorter content is simpler. If you're encoding a URL, consider using a shortener such as [goo.gl](https://goo.gl/) or [bit.ly](https://bitly.com/).

When scanning, consider the following:

- QR code orientation doesn't matter.
- Higher resolution video is better, but is more CPU intensive.
- Direct, orthogonal scanning is better than scanning at an angle.
- Blurry video greatly reduces scanner performance.
- Auto-focus can cause lags in detection as the camera adjusts focus. Consider disabling it or using a fixed-focus camera with the subject positioned at the focal point.
- Exposure adjustment on cameras can cause lags in detection. Consider disabling it or having a fixed white backdrop.

## Credits

Powered by two main components used almost as created by their authors:
 - [Instascan](https://github.com/schmich/instascan) for scanning QR codes with the integrated webcam
 - [QRCode.js](https://github.com/davidshimjs/qrcodejs) for generating QR codes

## License

Copyright &copy; 2018 Tudor P
Copyright &copy; 2016 Chris Schmich
Copyright &copy; 2012 davidshimjs

MIT License. See [LICENSE](LICENSE) for details.
